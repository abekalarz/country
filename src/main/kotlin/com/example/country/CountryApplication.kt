package com.example.country

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CountryApplication

fun main(args: Array<String>) {
	runApplication<CountryApplication>(*args)
}
