package com.example.country.api

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("/northcountries")
class CountryController {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getNorthernCountries() {

    }

}